/**
 * @file
 * Main JavaScript file.
 */

/**
 * Loading pop-up.
 */
function nice_popup_load() {
  jQuery('#wisepop-content').css("display", "block");
  jQuery('#fade').css("display", "block");
}

/**
 * Closing pop-up.
 */
function nice_popup_disable() {
  jQuery('#wisepop-content').css("display", "none");
  jQuery('#fade').css("display", "none");
}
/**
 * Display pop-up message.
 */
function nice_popup_display(nice_popup_body , border_color , nice_popup_width) {
  jQuery('body').append("<div id='fade' class='black_overlay'></div>");
  jQuery('body').append("<div id='wisepop-content' class='white_content'>" + nice_popup_body + "</div>");
  jQuery('.black_overlay').attr('style', 'display: none;position: fixed;top: 0%;left: 0%; width: 100%;height: 100%;background-color: black; z-index:1001;-moz-opacity: 0.8;opacity:.80; filter: alpha(opacity=80);');
  jQuery('.white_content').attr('style', 'display: none; position: absolute;top: 10%;left: 30%; width: ' + nice_popup_width + '; height: auto; padding: 16px;border: 10px solid ' + border_color + '; background-color: white;z-index:1002; border-radius: 13px;overflow: auto;text-align: center;');
  jQuery('#close-popup').attr('style', 'position:absolute; top:10px;right:10px;cursor: pointer;');
  nice_popup_load();

  // Closing pop-up.
  // Click the x event -inside message body.
  jQuery("#close-popup").click(function() { nice_popup_disable(); });
  // Click out event!
  jQuery("#fade").click(function() { nice_popup_disable(); });
}


Drupal.behaviors.nice_popup = {
  attach: function(context) {
    var now = new Date();
    var nowtimeSeconds = now.getTime() / 1000;
    var pop_up_cookie = jQuery.cookie("pop_up_displayed");
    var popup_expireAfter = Drupal.settings.nice_popup.js_expire_after;
    var popup_frequency = Drupal.settings.nice_popup.popup_frequency;
    var nice_popup_body = Drupal.settings.nice_popup.popup_body;
    var border_color = Drupal.settings.nice_popup.border_color;
    var nice_popup_width = Drupal.settings.nice_popup.nice_popup_width;

    if (popup_frequency == 'always') {
      // Delete the cookie and display the popup always.
      jQuery.cookie("pop_up_displayed", '' , {path: '/'});
      nice_popup_display(nice_popup_body , border_color , nice_popup_width);
    }
    else if (popup_frequency == 'never') {
      return;
    }
    else {
      if (jQuery.cookie("pop_up_displayed") < nowtimeSeconds) {
        jQuery.cookie("pop_up_displayed", nowtimeSeconds + popup_expireAfter , {path: '/'});
        nice_popup_display(nice_popup_body , border_color , nice_popup_width);
      }
    }
  }
};
