

INTRODUCTION
------------
 * Display a nice pop-up message for users once per browser session.

 
INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
 https://drupal.org/documentation/install/modules-themes/modules-7 
 for further information.

 
CONFIGURATION
-------------
 * Go to admin/config/user-interface/nice_popup and option for enable /disable
 Pop-up, set pop-up boarder color, set pop-up width in percentage and add custom
 text in pop-up body.-
 * Go to admin/people/permissions and set permissions.

    1. Plug and play module to any Drupal 7.x website.
    2. Popup enable /disable option . Set frequency of popup
	[off/always/once/daily/weekly].
    3. Set popup boarder color.
    4. Set popup with in percentage . Always use popup with in percentage
	to fit in any responsive layout.
    5. Text area to add pop-up message, with HTML support.
    6. Browser Cookie to manage pop-up to display only once in any
	browser (until user delete the browser cookie )
    7. Popup is not fixed in any location , so work in responsive design.
    8. For adding close button inside pop-up message use id="close-popup"
	with any HTML element.
	Example : <a href = "javascript:void(0)" id="close-popup" >X</a> or
	<img src ="/sites/all/modules/nice_popup/popup-close-button.png"
	alt="Close" id="close-popup" >


MAINTAINERS
-----------
Current maintainer:
 * Rabith Kuniyil - https://www.drupal.org/user/255559
