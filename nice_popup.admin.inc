<?php

/**
 * @file
 * Settings.
 */

/**
 * Settings form.
 *
 * @return array
 *   Form array.
 */
function nice_popup_settings() {
  $form = array();

  $form['nice_popup_frequency'] = array(
    '#type' => 'select',
    '#title' => t('How often should visitors see the nice popup?'),
    '#default_value' => variable_get('nice_popup_frequency', 'never'),
    '#options' => array(
      'never' => t('Never (off)'),
      'always' => t('Always'),
      'once' => t('Once'),
      'daily' => t('Daily'),
      'weekly' => t('Weekly'),
    ),
  );
  $form['nice_popup_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pop-up message settings'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['nice_popup_fieldset']['nice_popup_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Popup Border Color'),
    '#default_value' => variable_get('nice_popup_border_color', '#02b6e3'),
    '#description' => t('For setting border color of the pop-up ,depend  on the website theme.User color code with "#" . Example: #02b6e3 .  For getting the correct color code try <a target="_blank" href="http://www.colorpicker.com">http://www.colorpicker.com/</a>'),
    '#required' => TRUE,
  );
  $form['nice_popup_fieldset']['nice_popup_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Pop-up Width'),
    '#default_value' => variable_get('nice_popup_width', '30%'),
    '#required' => TRUE,
    '#description' => t('Use the pop-up width in % value . Example : 30% .'),
  );
  $nice_popup_body = variable_get('nice_popup_body');
  if ($nice_popup_body['value'] == '') {
    $nice_popup_body['value'] = '<div><img src ="/sites/all/modules/nice_popup/popup-close-button.png" alt="Close" id="close-popup" ><br /> Test Message</div>';
  }
  $form['nice_popup_fieldset']['nice_popup_body'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#title' => t('Pop-up Message body'),
    '#default_value' => $nice_popup_body['value'],
    '#format' => isset($nice_popup_body['format']) ? $nice_popup_body['format'] : NULL,
  );
  return system_settings_form($form);
}
